require 'zircon'
require 'concurrent'

class StartView < Zircon::View::Base::BaseView
  def onDock
    button = Zircon::Components
               .button
               .withAlignmentWithin(screen, Zircon::Component::ComponentAlignment::CENTER)
               .withText('Start')
               .build

    screen.addComponent(button)
    
    button.processComponentEvents(Zircon::UIEvent::ComponentEventType::ACTIVATED) do
      view_thread = Concurrent::Promises.future do
        Application['views.play']
      end
      button.set_hidden(true)
      p view_thread.reason
      replaceWith(view_thread.value)
    end
  end
end
