require 'zircon'
require 'game_config'
require 'game'

class PlayView < Zircon::View::Base::BaseView
  attr_reader :game

  def onDock
    sidebar = Zircon::Components
                .panel
                .withDecorations(Zircon::ComponentDecorations.box(Zircon::Graphics::BoxType::SINGLE))
                .withSize(GameConfig::SIDEBAR_WIDTH, GameConfig::WINDOW_HEIGHT)
                .build

    log_area = Zircon::Components
                 .logArea
                 .withDecorations(Zircon::ComponentDecorations.box(Zircon::Graphics::BoxType::DOUBLE, 'Log'))
                 .withSize(GameConfig::WINDOW_WIDTH - GameConfig::SIDEBAR_WIDTH, GameConfig::LOG_AREA_HEIGHT)
                 .withAlignmentWithin(screen, Zircon::Component::ComponentAlignment::BOTTOM_RIGHT)
                 .build

    screen.addComponent(sidebar)
    screen.addComponent(log_area)

    game = TheGame.create

    game_component = Zircon::GameComponents.newGameComponentBuilder
        .withGameArea(game.world.game_area)
        .withAlignmentWithin(screen, Zircon::Component::ComponentAlignment::TOP_RIGHT)
        .build
    
    game_panel = Zircon::Components.panel
                   .withSize(game.world.visible_size.to2DSize)
                   .withAlignmentWithin(screen, Zircon::Component::ComponentAlignment::TOP_RIGHT)
                   .build

    screen.addComponent(game_component)

    screen.handleKeyboardEvents(Zircon::UIEvent::KeyboardEventType::KEY_PRESSED) do |event|
      game.handle_keyboard_event(event)
      Zircon::UIEvent::UIEventResponse.processed
    end
  end
end
