require './system/import'

module Game
  class Block
    include ::Import['game.tiles']

    def initialize(default_tile_type, tiles:)
      tile = tiles.get(default_tile_type)
      @default_tile = tile
      @tiles_repo = tiles
      @entity = nil

      @zircon_block = build_block(tile)
      update!
    end

    def floor?
      @default_tile == @tiles_repo.get(:floor)
    end

    def add_entity(entity)
      @entity = entity
      update!
    end

    def remove_entity
      @entity = nil
      update!
    end

    def method_missing(m, *args, &block)
      @zircon_block.send(m, *args, &block)
    end
  
    def respond_to_missing?(m)
      @zircon_block.respond_to?(m)
    end

    private

    def build_block(default_tile)
      Zircon::Data::Block.newBuilder
        .withContent(default_tile)
        .withEmptyTile(@tiles_repo.get(:empty))
        .build
    end

    def update!
      # only supporting player for now
      if @entity
        @zircon_block.content = @entity.tile
      else
        @zircon_block.content = @default_tile
      end
    end
  end
end
