# frozen_string_literal: true

module Game
  class Tiles
    module Color
      WALL_FOREGROUND  = '#75715E'
      WALL_BACKGROUND  = '#3E3D32'
      FLOOR_FOREGROUND = '#75715E'
      FLOOR_BACKGROUND = '#1e2320'
      ACCENT_COLOR     = '#FFCD22'
    end

    class TypeDef
      attr_reader :character, :fg_color, :bg_color

      def initialize(character:, fg_color:, bg_color:)
        @character = character
        @bg_color = Zircon::Color::TileColor.fromString(bg_color)
        @fg_color = Zircon::Color::TileColor.fromString(fg_color)
      end
    end

    UnknownType = Class.new(StandardError)

    EMPTY = Zircon::Data::Tile.empty
    TYPE_DEFS = {
      wall: TypeDef.new(character: '#', fg_color: Color::WALL_FOREGROUND, bg_color: Color::WALL_BACKGROUND),
      floor: TypeDef.new(character: '.', fg_color: Color::FLOOR_FOREGROUND, bg_color: Color::FLOOR_BACKGROUND),
      player: TypeDef.new(character: '@', fg_color: Color::ACCENT_COLOR, bg_color: Color::FLOOR_BACKGROUND)
    }

    def get(type)
      type = type.to_sym
      return EMPTY if type == :empty
      raise UnknownType.new(type) unless TYPE_DEFS.include?(type)

      @types ||= {}
      @types[type] ||= begin
                         type_def = TYPE_DEFS[type]
                         bld = Zircon::Data::Tile.newBuilder

                          bld.withCharacter(type_def.character)

                           bld.withBackgroundColor(type_def.bg_color)
                           bld.withForegroundColor(type_def.fg_color)
                           bld.buildCharacterTile
                       end
    end
  end
end
