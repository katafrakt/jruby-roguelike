require './system/import'

module Game
  class Entity
    include ::Import['game.tiles']

    attr_reader :tile, :position

    def initialize(type, tiles:)
      @tile = tiles.get(type)
    end

    def position=(pos)
      @position = pos
    end
  end
end