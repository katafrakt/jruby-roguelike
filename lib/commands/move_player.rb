require 'commands/move_camera'

module Commands
  class MovePlayer
    attr_reader :x, :y

    def initialize(x, y)
      @x = x
      @y = y
    end

    def execute(world)
      player = world.player
      old_position = player.position
      old_block = world.fetchBlockAt(old_position).get
      new_position = player.position.withRelativeX(x).withRelativeY(y)
      new_block = world.fetchBlockAt(new_position).orElse(nil)
      return unless new_block

      if new_block.floor?
        old_block.remove_entity
        new_block.add_entity(player)
        player.position = new_position

        Commands::MoveCamera.new(player, old_position).execute(world)
      end
    end
  end
end