module Commands
  class MoveCamera
    attr_reader :player, :previous_position

    def initialize(player, previous_position)
      @player = player
      @previous_position = previous_position
    end

    def execute(world)
      screen_pos = player.position.minus(world.visibleOffset)
      half_height = world.visibleSize.yLength / 2
      half_width = world.visibleSize.xLength / 2
      current_position = player.position

      if previous_position.y > current_position.y && screen_pos.y < half_height
        world.scrollOneBackward
      end

      if previous_position.y < current_position.y && screen_pos.y > half_height
        world.scrollOneForward
      end

      if previous_position.x > current_position.x && screen_pos.x < half_width
        world.scrollOneLeft
      end
    end
  end
end