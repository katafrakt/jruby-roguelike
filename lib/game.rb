require 'world_builder'
require 'game_config'
require 'commands/move_player'

class TheGame
  attr_reader :world

  COORDS = {
    37 => [-1, 0],
    38 => [0, -1],
    39 => [1, 0],
    40 => [0, 1]
  }

  def self.create(world_size = GameConfig::WORLD_SIZE, visible_size = GameConfig::VISIBLE_SIZE)
    new(
      WorldBuilder.new(world_size)
        .make_caves
        .add_player(visible_size)
        .build(visible_size)
    )
  end

  def initialize(world)
    @world = world
  end

  def handle_keyboard_event(event)
    code = event.code.code
    Commands::MovePlayer.new(*COORDS[code]).execute(world) if COORDS[code]
  end
end
