require 'jbundler'
require 'java'

module Zircon
  include_package 'org.hexworks.zircon.api'

  Data = Module.new { include_package 'org.hexworks.zircon.api.data' }
  Data::Base = Module.new { include_package 'org.hexworks.zircon.api.data.base' }
  Application = Module.new { include_package 'org.hexworks.zircon.api.application' }
  Screen = Module.new { include_package 'org.hexworks.zircon.api.screen' }
  View = Module.new { include_package 'org.hexworks.zircon.api.view' }
  View::Base = Module.new { include_package 'org.hexworks.zircon.api.view.base' }
  Color = Module.new { include_package 'org.hexworks.zircon.api.color' }
  Graphics = Module.new { include_package 'org.hexworks.zircon.api.graphics' }
  UIEvent = Module.new { include_package 'org.hexworks.zircon.api.uievent' }
  Functions = Module.new { include_package 'org.hexworks.zircon.api.functions' }
  Component = Module.new { include_package 'org.hexworks.zircon.api.component' }
  Builder = Module.new { include_package 'org.hexworks.zircon.api.builder' }
  Builder::Game = Module.new { include_package 'org.hexworks.zircon.api.builder.game' }
  Game = Module.new { include_package 'org.hexworks.zircon.api.game' }
end
