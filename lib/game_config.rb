require 'zircon'

class GameConfig
  # dimensions
  WINDOW_WIDTH = 80
  WINDOW_HEIGHT = 50
  SIDEBAR_WIDTH = 18
  LOG_AREA_HEIGHT = 8

  # themes and colours
  TILESET = Zircon::CP437TilesetResources.rexPaint16x16()
  THEME = Zircon::ColorThemes.arc

  # world
  WORLD_SIZE = Zircon::Data::Size3D.create(WINDOW_WIDTH * 1.5, WINDOW_HEIGHT * 1.5, 1)
  VISIBLE_SIZE = Zircon::Data::Size3D.create(WINDOW_WIDTH - SIDEBAR_WIDTH, WINDOW_HEIGHT - LOG_AREA_HEIGHT, 1)
end
