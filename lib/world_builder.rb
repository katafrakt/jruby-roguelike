require 'game/block'
require 'game/entity'
require 'world'

class WorldBuilder
  def initialize(world_size)
    @world_size = world_size
    @width = world_size.xLength
    @height = world_size.zLength
    @blocks = {}
  end

  def make_caves
    randomize_tiles
    smooth(8)
    self
  end

  def add_player(visible_size)
    @player = Game::Entity.new(:player)
    loop do
      candidate_pos = blocks.keys.sample
      if visible_size.containsPosition(candidate_pos) && blocks[candidate_pos].floor?
        @player.position = candidate_pos
        blocks[candidate_pos].add_entity(@player)
        break
      end
    end

    self
  end

  def build(visible_size)
    World.new(blocks, visible_size, world_size, player)
  end

  private

  attr_reader :width, :height, :blocks, :world_size, :player

  def randomize_tiles
    world_size.fetchPositions.iterator.each do |pos|
      blocks[pos] = rand(2).zero? ? Game::Block.new(:floor) : Game::Block.new(:wall)
    end
  end

  def smooth(iters)
    new_blocks = {}
    iters.times do
      world_size.fetchPositions.iterator.each do |pos|
        x = pos.component1
        y = pos.component2
        z = pos.component3
        floors = 0
        rocks = 0

        (same_level_neighbors_shuffled(pos) + [pos]).each do |neighbor|
          next unless blocks[neighbor]

          blocks[neighbor].floor? ? floors += 1 : rocks += 1
        end
        position = Zircon::Data::Position3D.create(x, y, z)
        new_blocks[position] = floors >= rocks ? Game::Block.new(:floor) : Game::Block.new(:wall)
      end
    end
    @blocks = new_blocks
  end

  def same_level_neighbors_shuffled(pos)
    (-1..1).flat_map do |x|
      (-1..1).map do |y|
        pos.withRelativeX(x).withRelativeY(y)
      end
    end.reject{ |x| x == pos }.shuffle
  end
end
