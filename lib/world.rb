class World
  attr_reader :game_area, :player

  def initialize(starting_blocks, visible_size, actual_size, player)
    puts "Initializing World with visible size: #{visible_size} and actual size: #{actual_size}"
    @game_area = build_game_area(visible_size, actual_size, starting_blocks)
    @player = player
  end

  def method_missing(m, *args, &block)
    game_area.send(m, *args, &block)
  end

  def respond_to_missing?(m)
    game_area.respond_to?(m)
  end

  private

  def build_game_area(visible_size, actual_size, starting_blocks)
    builder = Zircon::GameComponents.newGameAreaBuilder
      .withVisibleSize(visible_size)
      .withActualSize(actual_size)

    puts "Setting blocks..."
    start = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    
    starting_blocks.each do |pos, block|
      builder.withBlock(pos, block)
    end
    
    elapsed = Process.clock_gettime(Process::CLOCK_MONOTONIC) - start
    puts "Created #{starting_blocks.length} blocks in #{elapsed} (#{starting_blocks.length/elapsed} blocks/s)"
      
    builder.build
  end
end
