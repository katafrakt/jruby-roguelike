require 'dry/system/container'

class Application < Dry::System::Container
  configure do |config|
    config.root = Pathname('.')
    config.auto_register('lib')
  end

  load_paths!('lib')
end

require 'zircon'
require 'game_config'

app_config = Zircon::Application::AppConfig.newBuilder()
               .withSize(Zircon::Data::Size.create(GameConfig::WINDOW_WIDTH, GameConfig::WINDOW_HEIGHT))
               .withDefaultTileset(GameConfig::TILESET)
               .build

app = Zircon::SwingApplications.startApplication(app_config)

Application.register('app', app)

require 'views/start'
Application.register('views.start') { StartView.new(app.tile_grid, GameConfig::THEME) }
Application.register('views.play', -> { require 'views/play'; PlayView.new(app.tile_grid, GameConfig::THEME) })

Application.register('game.tiles', -> { require 'game/tiles'; Game::Tiles.new })
